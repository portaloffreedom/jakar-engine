use std::sync::{Arc, RwLock};

use core::engine_settings;
use input::keymap::KeyMap;
use winit;

pub struct InputHandler {
    key_map: Arc<RwLock<KeyMap>>,
    settings: Arc<RwLock<engine_settings::EngineSettings>>,
    events_loop: winit::EventsLoop,
}


impl InputHandler{
    ///Creates a new input handler, needs to be started via `start` and ended via `end`
    pub fn new(
        key_map: Arc<RwLock<KeyMap>>,
        settings: Arc<RwLock<engine_settings::EngineSettings>>,

    ) -> Self{

        InputHandler{
            key_map: key_map,
            settings: settings,
            events_loop: winit::EventsLoop::new(),
        }
    }

    ///Starts the input reading and saves the current key-map for usage in everything input releated
    pub fn update_keys(&mut self){

        //Create a time which keeps track of the lst time to calculate the later

        //Create a tmp keymap which will overwrite the global keymap in `input`
        //for each iteration
        //let mut current_keys = KeyMap::new();
        let mut current_keys = self.key_map.read().expect("failed to lock source key map").clone();

        //Kill the axis motion for now
        current_keys.reset_data();

        let settings_copy = self.settings.clone();

        //Now do the events polling
        self.events_loop.poll_events(|ev| {
            match ev {
                //Check the event type
                //window
                winit::Event::WindowEvent{window_id, event} =>{
                    //Make life easier and temporarly import all the events
                    use winit::WindowEvent::*;

                    match event{
                        HiDpiFactorChanged(factor) => {
                            println!("HiDpiFactor changed to {}", factor);
                        },
                        Resized(new_pos) =>{

                            //Copy our selfs a settings instance to change settings which ... changed
                            let mut settings_instance = settings_copy.read()
                            .expect("failed to lock settings in input handler").clone();

                            let (width, height): (u32, u32) = new_pos.into();
                            //Check if we changed to something below 10x10, if so, cap it there
                            let mut used_width = width;
                            let mut used_height = height;
                            if used_width < 10{
                                used_width = 10;
                            }
                            if used_height < 10{
                                used_height = 10;
                            }

                            settings_instance.set_dimensions(
                                used_width as u32,
                                used_height as u32
                            );
                        },
                        Moved(new_location) =>{
                            println!("Moved to {:?}!", new_location);
                        },
                        Closed => {
                            //println!("Closed!", );
                            current_keys.closed = true;
                        },
                        DroppedFile(file_path) =>{
                            println!("Droped file with path: {:?}", file_path );
                        },
                        HoveredFile(file_path) => {
                            println!("Hovered file with path: {:?}", file_path );
                        },
                        HoveredFileCancelled => {
                            println!("Hovered file with path canceled");
                        },
                        ReceivedCharacter(character) =>{
                            println!("Recived Character: {:?}", character);
                        },
                        Focused(b_state) =>{
                            println!("Focused: state({:?})", b_state);
                        },
                        KeyboardInput {device_id, input} =>{
                            handle_keyboard_input(input, &mut current_keys);
                        },
                        CursorMoved {device_id, position, modifiers} =>{

                        },
                        CursorEntered{device_id} =>{

                        },
                        CursorLeft{device_id} =>{

                        },
                        MouseWheel{device_id, delta, phase, modifiers} =>{

                        },
                        MouseInput{device_id, state, button, modifiers} =>{
                            println!("MouseInput", );
                        },

                        TouchpadPressure{device_id, pressure, stage} =>{

                        },
                        AxisMotion{device_id, axis, value} =>{

                        },
                        Refresh =>{

                        },
                        Touch(touch) =>{

                        },
                    }
                },
                //Device
                winit::Event::DeviceEvent{device_id, event} => {
                    //Using raw events for the mouse movement
                    //One could, potentually use a 3rd axis for a 3d controller movement
                    //I think
                    match event{
                        winit::DeviceEvent::Added => {}
                        winit::DeviceEvent::Removed => {}
                        winit::DeviceEvent::MouseMotion{delta} => {}
                        winit::DeviceEvent::MouseWheel{delta} => {}

                        winit::DeviceEvent::Motion{axis, value} => {
                            //since winit 0.7.6
                            match axis {
                                0 => current_keys.mouse_delta_x = value, //Mouse x
                                1 => current_keys.mouse_delta_y = value, //mouse y
                                2 => {}, //Currently doing nothing, I guess this is mouse wheel

                                _ => {
                                    println!("Unknown Device Event Motion", );
                                },
                            }
                        }
                        winit::DeviceEvent::Button{button, state} => {
                            println!("Device Event Button!", );
                        }
                        winit::DeviceEvent::Key(keyboard) => {
                            handle_keyboard_input(keyboard, &mut current_keys);
                        }
                        winit::DeviceEvent::Text{codepoint} => {
                            println!("Device event Text!", );
                        }

                    }
                },
                //Awake (not implemented)
                winit::Event::Awakened => {},

                winit::Event::Suspended(state) => {},

            }
        });

        //Overwrite the Arc<RwLock<KeyMap>> with the new capture
        {
            let mut key_map_unlck = self.key_map
            .write()
            .expect("failed to hold key_map_inst lock while updating key info");
            (*key_map_unlck) = current_keys;
        }
    }

    ///Returns a reference to the events-loop used by this inputhandler
    pub fn get_events_loop(&self) -> &winit::EventsLoop{
        &self.events_loop
    }
}

fn handle_keyboard_input(input: winit::KeyboardInput, current_keys: &mut KeyMap){
    use winit::VirtualKeyCode;
    //Match the type of input
    match input.state{
        //if pressed set true, else leave false
        winit::ElementState::Pressed => {
            match input.virtual_keycode{
                //main keys
                Some(VirtualKeyCode::A) => current_keys.a = true,
                Some(VirtualKeyCode::B) => current_keys.b = true,
                Some(VirtualKeyCode::C) => current_keys.c = true,
                Some(VirtualKeyCode::D) => current_keys.d = true,
                Some(VirtualKeyCode::E) => current_keys.e = true,
                Some(VirtualKeyCode::F) => current_keys.f = true,
                Some(VirtualKeyCode::G) => current_keys.g = true,
                Some(VirtualKeyCode::H) => current_keys.h = true,
                Some(VirtualKeyCode::I) => current_keys.i = true,
                Some(VirtualKeyCode::J) => current_keys.j = true,
                Some(VirtualKeyCode::K) => current_keys.k = true,
                Some(VirtualKeyCode::L) => current_keys.l = true,
                Some(VirtualKeyCode::M) => current_keys.m = true,
                Some(VirtualKeyCode::N) => current_keys.n = true,
                Some(VirtualKeyCode::O) => current_keys.o = true,
                Some(VirtualKeyCode::P) => current_keys.p = true,
                Some(VirtualKeyCode::Q) => current_keys.q = true,
                Some(VirtualKeyCode::R) => current_keys.r = true,
                Some(VirtualKeyCode::S) => current_keys.s = true,
                Some(VirtualKeyCode::T) => current_keys.t = true,
                Some(VirtualKeyCode::U) => current_keys.u = true,
                Some(VirtualKeyCode::V) => current_keys.v = true,
                Some(VirtualKeyCode::W) => current_keys.w = true,
                Some(VirtualKeyCode::X) => current_keys.x = true,
                Some(VirtualKeyCode::Y) => current_keys.y = true,
                Some(VirtualKeyCode::Z) => current_keys.z = true,
                //top numbers
                Some(VirtualKeyCode::Key1) => current_keys.t_1 = true,
                Some(VirtualKeyCode::Key2) => current_keys.t_2 = true,
                Some(VirtualKeyCode::Key3) => current_keys.t_3 = true,
                Some(VirtualKeyCode::Key4) => current_keys.t_4 = true,
                Some(VirtualKeyCode::Key5) => current_keys.t_5 = true,
                Some(VirtualKeyCode::Key6) => current_keys.t_6 = true,
                Some(VirtualKeyCode::Key7) => current_keys.t_7 = true,
                Some(VirtualKeyCode::Key8) => current_keys.t_8 = true,
                Some(VirtualKeyCode::Key9) => current_keys.t_9 = true,
                Some(VirtualKeyCode::Key0) => current_keys.t_0 = true,
                //num pad
                Some(VirtualKeyCode::Numpad0) => current_keys.num_0 = true,
                Some(VirtualKeyCode::Numpad1) => current_keys.num_1 = true,
                Some(VirtualKeyCode::Numpad2) => current_keys.num_2 = true,
                Some(VirtualKeyCode::Numpad3) => current_keys.num_3 = true,
                Some(VirtualKeyCode::Numpad4) => current_keys.num_4 = true,
                Some(VirtualKeyCode::Numpad5) => current_keys.num_5 = true,
                Some(VirtualKeyCode::Numpad6) => current_keys.num_6 = true,
                Some(VirtualKeyCode::Numpad7) => current_keys.num_7 = true,
                Some(VirtualKeyCode::Numpad8) => current_keys.num_8 = true,
                Some(VirtualKeyCode::Numpad9) => current_keys.num_9 = true,
                //F-keys
                Some(VirtualKeyCode::F1) => current_keys.f1 = true,
                Some(VirtualKeyCode::F2) => current_keys.f2 = true,
                Some(VirtualKeyCode::F3) => current_keys.f3 = true,
                Some(VirtualKeyCode::F4) => current_keys.f4 = true,
                Some(VirtualKeyCode::F5) => current_keys.f5 = true,
                Some(VirtualKeyCode::F6) => current_keys.f6 = true,
                Some(VirtualKeyCode::F7) => current_keys.f7 = true,
                Some(VirtualKeyCode::F8) => current_keys.f8 = true,
                Some(VirtualKeyCode::F9) => current_keys.f9 = true,
                Some(VirtualKeyCode::F10) => current_keys.f10 = true,
                Some(VirtualKeyCode::F11) => current_keys.f11 = true,
                Some(VirtualKeyCode::F12) => current_keys.f12 = true,
                Some(VirtualKeyCode::F13) => current_keys.f13 = true,
                Some(VirtualKeyCode::F14) => current_keys.f14 = true,
                Some(VirtualKeyCode::F15) => current_keys.f15 = true,
                //special keys
                Some(VirtualKeyCode::LControl) => current_keys.ctrl_l = true,
                Some(VirtualKeyCode::RControl) => current_keys.ctrl_r = true,
                Some(VirtualKeyCode::LAlt) => current_keys.alt_l = true,
                Some(VirtualKeyCode::RAlt) => current_keys.alt_r = true,
                Some(VirtualKeyCode::LWin) => current_keys.super_l = true,
                Some(VirtualKeyCode::RWin) => current_keys.super_r = true,
                Some(VirtualKeyCode::Capital) => current_keys.caps_lock = true,
                Some(VirtualKeyCode::LShift) => current_keys.shift_l = true,
                Some(VirtualKeyCode::RShift) => current_keys.shift_r = true,
                Some(VirtualKeyCode::Tab) => current_keys.tab = true,
                Some(VirtualKeyCode::Space) => current_keys.space = true,
                Some(VirtualKeyCode::Return) => current_keys.enter = true,
                Some(VirtualKeyCode::NumpadEnter) => current_keys.nume_enter = true,
                Some(VirtualKeyCode::Escape) => current_keys.escape = true,
                //arrows
                Some(VirtualKeyCode::Up) => current_keys.up = true,
                Some(VirtualKeyCode::Down) => current_keys.down = true,
                Some(VirtualKeyCode::Left) => current_keys.left = true,
                Some(VirtualKeyCode::Right) => current_keys.right = true,
                _ => {println!("Unknown KeyCode", );},
            }
        },
        winit::ElementState::Released => {
            //leave state to false
            match input.virtual_keycode{

                Some(VirtualKeyCode::A) => current_keys.a = false,
                Some(VirtualKeyCode::B) => current_keys.b = false,
                Some(VirtualKeyCode::C) => current_keys.c = false,
                Some(VirtualKeyCode::D) => current_keys.d = false,
                Some(VirtualKeyCode::E) => current_keys.e = false,
                Some(VirtualKeyCode::F) => current_keys.f = false,
                Some(VirtualKeyCode::G) => current_keys.g = false,
                Some(VirtualKeyCode::H) => current_keys.h = false,
                Some(VirtualKeyCode::I) => current_keys.i = false,
                Some(VirtualKeyCode::J) => current_keys.j = false,
                Some(VirtualKeyCode::K) => current_keys.k = false,
                Some(VirtualKeyCode::L) => current_keys.l = false,
                Some(VirtualKeyCode::M) => current_keys.m = false,
                Some(VirtualKeyCode::N) => current_keys.n = false,
                Some(VirtualKeyCode::O) => current_keys.o = false,
                Some(VirtualKeyCode::P) => current_keys.p = false,
                Some(VirtualKeyCode::Q) => current_keys.q = false,
                Some(VirtualKeyCode::R) => current_keys.r = false,
                Some(VirtualKeyCode::S) => current_keys.s = false,
                Some(VirtualKeyCode::T) => current_keys.t = false,
                Some(VirtualKeyCode::U) => current_keys.u = false,
                Some(VirtualKeyCode::V) => current_keys.v = false,
                Some(VirtualKeyCode::W) => current_keys.w = false,
                Some(VirtualKeyCode::X) => current_keys.x = false,
                Some(VirtualKeyCode::Y) => current_keys.y = false,
                Some(VirtualKeyCode::Z) => current_keys.z = false,
                //top numbers
                Some(VirtualKeyCode::Key1) => current_keys.t_1 = false,
                Some(VirtualKeyCode::Key2) => current_keys.t_2 = false,
                Some(VirtualKeyCode::Key3) => current_keys.t_3 = false,
                Some(VirtualKeyCode::Key4) => current_keys.t_4 = false,
                Some(VirtualKeyCode::Key5) => current_keys.t_5 = false,
                Some(VirtualKeyCode::Key6) => current_keys.t_6 = false,
                Some(VirtualKeyCode::Key7) => current_keys.t_7 = false,
                Some(VirtualKeyCode::Key8) => current_keys.t_8 = false,
                Some(VirtualKeyCode::Key9) => current_keys.t_9 = false,
                Some(VirtualKeyCode::Key0) => current_keys.t_0 = false,
                //F-keys
                Some(VirtualKeyCode::F1) => current_keys.f1 = false,
                Some(VirtualKeyCode::F2) => current_keys.f2 = false,
                Some(VirtualKeyCode::F3) => current_keys.f3 = false,
                Some(VirtualKeyCode::F4) => current_keys.f4 = false,
                Some(VirtualKeyCode::F5) => current_keys.f5 = false,
                Some(VirtualKeyCode::F6) => current_keys.f6 = false,
                Some(VirtualKeyCode::F7) => current_keys.f7 = false,
                Some(VirtualKeyCode::F8) => current_keys.f8 = false,
                Some(VirtualKeyCode::F9) => current_keys.f9 = false,
                Some(VirtualKeyCode::F10) => current_keys.f10 = false,
                Some(VirtualKeyCode::F11) => current_keys.f11 = false,
                Some(VirtualKeyCode::F12) => current_keys.f12 = false,
                Some(VirtualKeyCode::F13) => current_keys.f13 = false,
                Some(VirtualKeyCode::F14) => current_keys.f14 = false,
                Some(VirtualKeyCode::F15) => current_keys.f15 = false,
                //num pad
                Some(VirtualKeyCode::Numpad0) => current_keys.num_0 = false,
                Some(VirtualKeyCode::Numpad1) => current_keys.num_1 = false,
                Some(VirtualKeyCode::Numpad2) => current_keys.num_2 = false,
                Some(VirtualKeyCode::Numpad3) => current_keys.num_3 = false,
                Some(VirtualKeyCode::Numpad4) => current_keys.num_4 = false,
                Some(VirtualKeyCode::Numpad5) => current_keys.num_5 = false,
                Some(VirtualKeyCode::Numpad6) => current_keys.num_6 = false,
                Some(VirtualKeyCode::Numpad7) => current_keys.num_7 = false,
                Some(VirtualKeyCode::Numpad8) => current_keys.num_8 = false,
                Some(VirtualKeyCode::Numpad9) => current_keys.num_9 = false,
                //special keys
                Some(VirtualKeyCode::LControl) => current_keys.ctrl_l = false,
                Some(VirtualKeyCode::RControl) => current_keys.ctrl_r = false,
                Some(VirtualKeyCode::LAlt) => current_keys.alt_l = false,
                Some(VirtualKeyCode::RAlt) => current_keys.alt_r = false,
                Some(VirtualKeyCode::LWin) => current_keys.super_l = false,
                Some(VirtualKeyCode::RWin) => current_keys.super_r = false,
                Some(VirtualKeyCode::Capital) => current_keys.caps_lock = false,
                Some(VirtualKeyCode::LShift) => current_keys.shift_l = false,
                Some(VirtualKeyCode::RShift) => current_keys.shift_r = false,
                Some(VirtualKeyCode::Tab) => current_keys.tab = false,
                Some(VirtualKeyCode::Space) => current_keys.space = false,
                Some(VirtualKeyCode::Return) => current_keys.enter = false,
                Some(VirtualKeyCode::NumpadEnter) => current_keys.nume_enter = false,
                Some(VirtualKeyCode::Escape) => current_keys.escape = false,
                //arrows
                Some(VirtualKeyCode::Up) => current_keys.up = false,
                Some(VirtualKeyCode::Down) => current_keys.down = false,
                Some(VirtualKeyCode::Left) => current_keys.left = false,
                Some(VirtualKeyCode::Right) => current_keys.right = false,

                _ => {println!("Unknown KeyCode", );},
            }
        },
    }
}
