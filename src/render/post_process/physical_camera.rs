use render::shader::shaders::default_pstprg_fragment;
use render::pipeline;
use render::pipeline_manager;
use render::pipeline_builder;
use render::render_passes::RenderPassConf;
use render::frame_system::FrameSystem;
use core::engine_settings;
use core::resource_management::asset_manager::AssetManager;
use render::render_passes::UsedFormats;
use render::renderer::{JkQueues, JkCommandBuff};

use vulkano;
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::buffer::cpu_pool::CpuBufferPool;
use vulkano::buffer::immutable::ImmutableBuffer;
use vulkano::sampler::Sampler;
use vulkano::descriptor::descriptor_set::FixedSizeDescriptorSetsPool;
use vulkano::buffer::DeviceLocalBuffer;
use vulkano::buffer::BufferUsage;
use vulkano::image::traits::{ImageAccess, ImageViewAccess};
use vulkano::sampler::Filter;
use vulkano::sampler::MipmapMode;
use vulkano::sampler::SamplerAddressMode;
use vulkano::image::ImageDimensions;
use vulkano::command_buffer::AutoCommandBufferBuilder;

use std::sync::{Arc, RwLock};

///Is able to determin the currently needed exposure for a nice image brightness,
/// It uses a histogram to hide brighness spikes
pub struct PhysicalCameraPass {
    engine_settings: Arc<RwLock<engine_settings::EngineSettings>>,
    //Used for the average compute pass
    average_pipe: Arc<vulkano::pipeline::ComputePipelineAbstract + Send + Sync>,
    ///Stores the lumiosity histogram as well as the currently needed exposure
    pub average_buffer: Arc<DeviceLocalBuffer<phys_cam_compute_shader::ty::LumiosityBuffer>>,
    average_set_pool: FixedSizeDescriptorSetsPool<Arc<vulkano::pipeline::ComputePipelineAbstract + Send + Sync>>,
    exposure_settings_pool: CpuBufferPool<phys_cam_compute_shader::ty::ExposureSettings>,
    screen_sampler: Arc<Sampler>,

    //Simple switch which is used to flood the deviceBuffer once with values
    first_invoke: bool,
}

impl PhysicalCameraPass{
    pub fn new(
        engine_settings: Arc<RwLock<engine_settings::EngineSettings>>,
        device: Arc<vulkano::device::Device>,
        queue: JkQueues,
        formats: UsedFormats,
        screen_sampler: Arc<Sampler>,
    ) -> Self{
        //The compute stuff...
        let compute_shader = Arc::new(phys_cam_compute_shader::Shader::load(device.clone())
            .expect("failed to create compute shader module"));

        let average_pipe: Arc<vulkano::pipeline::ComputePipelineAbstract + Send + Sync> = Arc::new(
            vulkano::pipeline::ComputePipeline::new(
                device.clone(), &compute_shader.main_entry_point(), &()
            ).expect("failed to build average pipeline")
        );

        let average_buffer = DeviceLocalBuffer::new(
            device.clone(), BufferUsage::all(), queue.get_families().into_iter()
        ).expect("failed to create average lumiosity buffer!");

        let average_set_pool = FixedSizeDescriptorSetsPool::new(average_pipe.clone(), 0);

        let exp_set_pool = CpuBufferPool::<phys_cam_compute_shader::ty::ExposureSettings>::new(
            device.clone(), vulkano::buffer::BufferUsage::all());

        PhysicalCameraPass{
            engine_settings: engine_settings,
            average_pipe: average_pipe,
            average_buffer: average_buffer,
            average_set_pool: average_set_pool,
            exposure_settings_pool: exp_set_pool,
            screen_sampler: screen_sampler,
            first_invoke: true,
        }
    }


    ///Takes the hdr_image computes the average lumiosity and stores it in its buffer. The information is used
    /// in the assamble stage to set the exposure setting.
    pub fn compute_lumiosity(&mut self,
        command_buffer: &mut JkCommandBuff,
        frame_system: &FrameSystem,
    ){

        //First, blit our final shaded image down to the one pix image. Then start the compute shader with
        //the eye adaption algorithm.
        let local_cb = command_buffer.get_graphics_buffer(); //Use the graphics queue for bluring

        let diffuse_image = frame_system.get_passes().images.ray_tracing.diffuse.clone();
        let camera_settings = self.engine_settings.read().expect("failed to lock settings")
        .get_camera_settings().clone();

        let exposure_data = phys_cam_compute_shader::ty::ExposureSettings{
            first_invoke: if self.first_invoke{self.first_invoke = false; 1}else{0}, //only set on the first run
            use_auto_exposure: if camera_settings.use_auto_exposure {1}else{0},
            sensitivity: camera_settings.sensitivity,
            aperture: camera_settings.aperture,
            shutter_speed: camera_settings.shutter_speed,
        };

        let exposure_settings_data = self.exposure_settings_pool.next(exposure_data)
        .expect("failed to allocate new exposure settings data");


        let des_set = self.average_set_pool.next()
            .add_sampled_image(
                diffuse_image,
                self.screen_sampler.clone()
            ).expect("failed to add sampled screen image")
            .add_buffer(self.average_buffer.clone())
            .expect("failed to add average buffer to compute descriptor")
            .add_buffer(exposure_settings_data)
            .expect("failed to add exposure settings to descriptor")
        .build().expect("failed to build average compute descriptor");

        //Start the compute operation
        //Only one thread...
        //TODO can we exploid the gpu to do some more fancy stuff?
        let new_cb = local_cb.dispatch([1, 1, 1], self.average_pipe.clone(), des_set, ())
        .expect("failed to add compute operation for average lumiosity");
        //Update the local buffer to this one
        command_buffer.set_graphics_buffer(new_cb);
    }
}

///The compute shader used to compute the current average lumiosity of this image
pub mod phys_cam_compute_shader{
    vulkano_shaders::shader!{
        ty: "compute",
        path: "data/shader/physical_camera_pass.comp"
    }
}
