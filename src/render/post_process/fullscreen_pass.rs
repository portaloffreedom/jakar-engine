
use core::resource_management::asset_manager::AssetManager;
use core::resources::camera::Camera;
use core::engine_settings::EngineSettings;

use render::pipeline::Pipeline;
use render::pipeline_builder::{PipelineConfig, DepthStencilConfig};
use render::render_passes::RenderPassConf;
use render::frame_system::FrameSystem;
use render::pipeline_manager::PipelineManager;
use render::shader::shaders::ambient_occlusion;
use render::shader::shaders::ao_finalize;
use render::render_passes::UsedFormats;
use render::renderer::JkCommandBuff;

use vulkano::device::Device;
use vulkano::command_buffer::AutoCommandBufferBuilder;
use vulkano::buffer::cpu_pool::CpuBufferPool;
use vulkano::descriptor::descriptor_set::FixedSizeDescriptorSetsPool;
use vulkano::pipeline::GraphicsPipelineAbstract;
use vulkano::sampler::Sampler;
use vulkano::buffer::BufferAccess;

use std::sync::{Arc, RwLock};
use cgmath::SquareMatrix;


///Collects several fullscreen passes, ranging from AO calculation over SSR to simple addition of
///two images
pub struct FullscreenPass {
    setting: Arc<RwLock<EngineSettings>>,

    ambient_occlusion_pipeline: Arc<Pipeline>,
    ao_fin_pipeline: Arc<Pipeline>,

    ao_desc_pool: FixedSizeDescriptorSetsPool<Arc<GraphicsPipelineAbstract + Send + Sync>>,
    ao_fin_desc_pool: FixedSizeDescriptorSetsPool<Arc<GraphicsPipelineAbstract + Send + Sync>>,

    ao_settings_pool: CpuBufferPool<ambient_occlusion::ty::ssao_settings>,
    ao_fin_settings_pool: CpuBufferPool<ao_finalize::ty::ssao_fin_settings>,

    screen_vertex_buffer: Arc<BufferAccess + Send + Sync>,
    screen_sampler: Arc<Sampler>,
}



impl FullscreenPass{
    pub fn new(
        device: Arc<Device>,
        setting: Arc<RwLock<EngineSettings>>,
        pipeline_manager: Arc<RwLock<PipelineManager>>,
        screen_vertex_buffer: Arc<BufferAccess + Send + Sync>,
        screen_sampler: Arc<Sampler>,
        formats: UsedFormats,
    ) -> Self{

        let ambient_occlusion_pipeline = pipeline_manager.write()
        .expect("failed to lock new pipeline manager")
        .get_pipeline_by_config(
            PipelineConfig::default()
                .with_shader("PpAo".to_string())
                .with_render_pass(RenderPassConf::SimplePass(formats.ambient))
                .with_depth_and_stencil_settings(
                    DepthStencilConfig::NoDepthNoStencil
                ),
        );


        let ao_fin_pipeline = pipeline_manager.write()
        .expect("failed to lock new pipeline manager")
        .get_pipeline_by_config(
            PipelineConfig::default()
                .with_shader("PpAoFinalize".to_string())
                .with_render_pass(RenderPassConf::SimplePass(formats.ambient))
                .with_depth_and_stencil_settings(
                    DepthStencilConfig::NoDepthNoStencil
                ),
        );


        let ao_desc_pool = FixedSizeDescriptorSetsPool::new(
            ambient_occlusion_pipeline.get_pipeline_ref(), 0
        );

        let ao_fin_desc_pool = FixedSizeDescriptorSetsPool::new(
            ao_fin_pipeline.get_pipeline_ref(), 0
        );

        let ao_settings_pool = CpuBufferPool::uniform_buffer(device.clone());
        let ao_fin_settings_pool = CpuBufferPool::uniform_buffer(device.clone());

        FullscreenPass{
            setting,

            ambient_occlusion_pipeline,
            ao_fin_pipeline,

            ao_desc_pool,
            ao_fin_desc_pool,

            ao_settings_pool,
            ao_fin_settings_pool,

            screen_vertex_buffer,
            screen_sampler,
        }
    }

    pub fn do_ssao(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        framesystem: &FrameSystem,
        asset_manager: &mut AssetManager
    ){
        let ao_image = framesystem.get_passes().images.post_images.raw_ao.clone();
        let framebuffer = framesystem.get_passes_mut().get_framebuff_simple(ao_image);

        let clearing = vec![
        [1.0].into(),
        ];

        //Start the render pass
        let mut new_cb = command_buffer.get_graphics_buffer().begin_render_pass(framebuffer, false, clearing)
        .expect("failed to start renderpass");

        let projection_matrix = asset_manager.get_camera().get_projection();
        let view_matrix = asset_manager.get_camera().get_view_matrix();
        let near_far = {
            let settings = asset_manager.get_camera().get_near_far();
            [settings.near_plane, settings.far_plane]
        };

        let ssao_settings = self.setting.read().expect("failed to lock settings")
        .get_render_settings().get_ssao();

        let current_settings = ambient_occlusion::ty::ssao_settings{
            projection: projection_matrix.into(),
            invProjection: projection_matrix.invert()
            .expect("failed to generate inv camera projection matrix").into(),
            view: view_matrix.into(),
            kernel_size: ssao_settings.sample_count as i32,
            kernel_radius: ssao_settings.kernel_radius,
            near_far: near_far,
        };

        //Construct settings
        let settings = self.ao_settings_pool.next(
            current_settings
        ).expect("failed to get next ssao settings");

        //Now create the descriptorset which gives it the info
        let depth_image = framesystem.get_passes().images.gbuffer.depth.clone();
        let gbuffer_normal = framesystem.get_passes().images.gbuffer.normal.clone();
        let gbuffer_ambient= framesystem.get_passes().images.gbuffer.ambient.clone();
        let desciptor = self.ao_desc_pool.next()
        .add_sampled_image(
            depth_image,
            self.screen_sampler.clone()
        )
        .expect("Failed to add AO depth source")
        .add_sampled_image(
            gbuffer_normal,
            self.screen_sampler.clone()
        )
        .expect("failed to add gbuffer normal image")
        .add_sampled_image(
            gbuffer_ambient,
            self.screen_sampler.clone()
        )
        .expect("failed to add gbuffer ao image")
        .add_buffer(settings)
        .expect("failed to add ssao settings!")
        .build()
        .expect("failed to build ao descriptor");

        //now execute pass and end
        new_cb = new_cb.draw(
            self.ambient_occlusion_pipeline.get_pipeline_ref(),
            framesystem.get_dynamic_state(),
            vec![self.screen_vertex_buffer.clone()],
            desciptor,
            ()
        ).expect("failed to add draw call for the AO post progress plane");

        new_cb = new_cb.end_render_pass().expect("failed to end ao pass");

        //Now add both passes together
        new_cb = self.finalize_ao(new_cb, framesystem);


        command_buffer.set_graphics_buffer(new_cb);
    }

    ///Adds the ssao to the curren opaque image
    pub fn finalize_ao(
        &mut self,
        command_buffer: AutoCommandBufferBuilder,
        framesystem: &FrameSystem
    ) -> AutoCommandBufferBuilder{

        let opaque_plus_ao = framesystem.get_passes().images.post_images.final_ao.clone();
        let framebuffer = framesystem.get_passes_mut().get_framebuff_simple(opaque_plus_ao);

        //TODO clear correctly
        let clearing = vec![
            [1.0].into(),
        ];
        //Start the render pass
        let mut new_cb = command_buffer.begin_render_pass(framebuffer, false, clearing)
        .expect("failed to start renderpass");

        //Get the settings
        let ssao_settings = {
            self.setting.read().expect("failed to lock settings").get_render_settings().get_ssao()
        };

        let settings = ao_finalize::ty::ssao_fin_settings{
            ssao_brightness: ssao_settings.intensity,
        };

        let settings_buf = self.ao_fin_settings_pool.next(settings).expect("failed to get ao add buffer");

        let ao_image = framesystem.get_passes().images.post_images.raw_ao.clone();

        let desc_set = self.ao_fin_desc_pool.next()
        .add_sampled_image(
            ao_image, self.screen_sampler.clone()
        )
        .expect("failed to add ssao image to ao add")
        .add_buffer(settings_buf)
        .expect("failed to add ao add settings to desc")
        .build().expect("failed to build ao add desc");

        //now execute pass and end
        new_cb = new_cb.draw(
            self.ao_fin_pipeline.get_pipeline_ref(),
            framesystem.get_dynamic_state(),
            vec![self.screen_vertex_buffer.clone()],
            desc_set,
            ()
        ).expect("failed to add draw call for the AO ADD post progress plane");

        new_cb = new_cb.end_render_pass().expect("failed to end ao pass");
        new_cb
    }

    ///finds the exposure setting based on the current camera and its settings.
    pub fn find_exposure(&self){

    }
}
