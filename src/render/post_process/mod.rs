
use render::shader::shaders::default_pstprg_fragment;
use render::pipeline;
use render::pipeline_manager;
use render::pipeline_builder;
use render::render_passes::RenderPassConf;
use render::frame_system::FrameSystem;
use core::engine_settings;
use core::resource_management::asset_manager::AssetManager;
use render::render_passes::UsedFormats;
use render::renderer::{JkQueues, JkCommandBuff};

use vulkano;
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::buffer::cpu_pool::CpuBufferPool;
use vulkano::buffer::immutable::ImmutableBuffer;
use vulkano::sampler::Sampler;
use vulkano::descriptor::descriptor_set::FixedSizeDescriptorSetsPool;
use vulkano::buffer::DeviceLocalBuffer;
use vulkano::buffer::BufferUsage;
use vulkano::image::traits::{ImageAccess, ImageViewAccess};
use vulkano::sampler::Filter;
use vulkano::sampler::MipmapMode;
use vulkano::sampler::SamplerAddressMode;
use vulkano::image::ImageDimensions;
use vulkano::command_buffer::AutoCommandBufferBuilder;



use std::sync::{Arc, RwLock};

///A module handling the generation of the final bloom image
pub mod bloom;
///Handels several cpuside easy fullscreen renderpasses.
pub mod fullscreen_pass;

///Contains a physical camera pass which will find the needed exposure setting for the current frame
pub mod physical_camera;

///Should be used in screenspace
#[derive(Clone,Copy)]
pub struct PostProcessVertex {
    position: [f32; 2],
    tex_coord: [f32; 2],
}

impl PostProcessVertex{
    pub fn new(pos: [f32; 2], uv: [f32;2]) -> Self{
        PostProcessVertex {
            position: pos,
            tex_coord: uv,
        }
    }
}

//Implements the vulkano::vertex trait on Vertex
impl_vertex!(PostProcessVertex, position, tex_coord);


///Is able to perform the post progressing on a command buffer based on a stored pipeline
pub struct PostProcess{
    engine_settings: Arc<RwLock<engine_settings::EngineSettings>>,

    ///Handles bloom
    bloom_system: bloom::Bloom,
    ///Handels the physical camera
    physical_camera: physical_camera::PhysicalCameraPass,

    ///Handles other screen posprogress like SSAO
    fullscreen_system: fullscreen_pass::FullscreenPass,
    pipeline: Arc<pipeline::Pipeline>,
    screen_vertex_buffer: Arc<vulkano::buffer::BufferAccess + Send + Sync>,
    screen_sampler: Arc<Sampler>,
    hdr_settings_pool: CpuBufferPool<default_pstprg_fragment::ty::hdr_settings>,
}


impl PostProcess{
    ///Create the postprogressing chain
    pub fn new(
        engine_settings: Arc<RwLock<engine_settings::EngineSettings>>,
        device: Arc<vulkano::device::Device>,
        queue: JkQueues,
        pipeline_manager: Arc<RwLock<pipeline_manager::PipelineManager>>,
        formats: UsedFormats,
    ) -> Self{
        //generate a vertex buffer
        let mut vertices: Vec<PostProcessVertex> = Vec::new();
        //the screen space vertexes
        vertices.push(PostProcessVertex::new([-1.0; 2], [0.0; 2]));
        vertices.push(PostProcessVertex::new([-1.0, 1.0], [0.0, 1.0]));
        vertices.push(PostProcessVertex::new([1.0; 2], [1.0; 2]));

        vertices.push(PostProcessVertex::new([-1.0; 2], [0.0; 2]));
        vertices.push(PostProcessVertex::new([1.0, -1.0], [1.0, 0.0]));
        vertices.push(PostProcessVertex::new([1.0; 2], [1.0; 2]));

        //Create the assemble pipeline
        let post_process_pipeline = pipeline_manager.write()
        .expect("failed to lock new pipeline manager")
        .get_pipeline_by_config(
            pipeline_builder::PipelineConfig::default()
                .with_shader("PpExposure".to_string())
                .with_render_pass(RenderPassConf::AssemblePass)
                .with_depth_and_stencil_settings(
                    pipeline_builder::DepthStencilConfig::NoDepthNoStencil
                ),
        );


        let (sample_vertex_buffer, buffer_future) = ImmutableBuffer::from_iter(
            vertices.iter().cloned(),
            vulkano::buffer::BufferUsage::all(),
            queue.transfer.clone()
        ).expect("failed to create buffer");
        //drop the future to wait for the upload.
        drop(buffer_future);

        //we also have to maintain a buffer pool for the settings which can potentually change
        let hdr_settings_pool = CpuBufferPool::<default_pstprg_fragment::ty::hdr_settings>::new(
            device.clone(), vulkano::buffer::BufferUsage::all()
        );

        let screen_sampler = Sampler::new(
            device.clone(),
            Filter::Linear,
            Filter::Linear,
            MipmapMode::Linear,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            SamplerAddressMode::ClampToEdge,
            0.0,
            1.0,
            1.0,
            1.0,
        ).expect("failed to create screen sampler");

        let fullscreen_system = fullscreen_pass::FullscreenPass::new(
            device.clone(),
            engine_settings.clone(),
            pipeline_manager.clone(),
            sample_vertex_buffer.clone(),
            screen_sampler.clone(),
            formats
        );

        PostProcess{
            engine_settings: engine_settings.clone(),

            bloom_system: bloom::Bloom::new(
                engine_settings.clone(),
                device.clone(),
                pipeline_manager.clone(),
                sample_vertex_buffer.clone(),
                screen_sampler.clone(),
                formats
            ),

            physical_camera: physical_camera::PhysicalCameraPass::new(
                engine_settings,
                device,
                queue.clone(),
                formats,
                screen_sampler.clone()
            ),

            fullscreen_system,

            pipeline: post_process_pipeline,


            screen_vertex_buffer: sample_vertex_buffer,


            screen_sampler: screen_sampler,
            hdr_settings_pool: hdr_settings_pool,

        }
    }

    pub fn get_hdr_settings(&self) -> vulkano::buffer::cpu_pool::CpuBufferPoolSubbuffer
    <default_pstprg_fragment::ty::hdr_settings, Arc<vulkano::memory::pool::StdMemoryPool>> {
        //Might add screen extend
        let (gamma, msaa, show_mode_int, far, near, shadow_idx) = {
            let es_lck = self.engine_settings
            .read()
            .expect("failed to lock settings for frame creation");

            let gamma = es_lck.get_render_settings().get_gamma();
            let msaa = es_lck.get_render_settings().get_msaa_factor();
            let debug_int = es_lck.get_render_settings().get_debug_settings().debug_view.as_shader_int();
            let far_plane = es_lck.camera.far_plane.clone();
            let near_plane = es_lck.camera.near_plane.clone();

            let shadow_idx = {
                let int = es_lck.get_render_settings().get_debug_settings().ldr_debug_view_level;
                if int  > (es_lck.get_render_settings().get_light_settings().shadow_settings.max_casting_lights as u32){
                    (es_lck.get_render_settings().get_light_settings().shadow_settings.max_casting_lights - 1)  as i32
                }else{
                    int as i32
                }
            };

            (gamma, msaa, debug_int, far_plane, near_plane, shadow_idx)
        };

        let hdr_settings_data = default_pstprg_fragment::ty::hdr_settings{
              gamma: gamma,
              sampling_rate: msaa as i32,
              show_mode: show_mode_int,
              near: near,
              far: far,
              shadow_idx: shadow_idx,
        };

        //the settings for this pass
        self.hdr_settings_pool.next(hdr_settings_data).expect("failed to alloc HDR settings")
    }

    ///Changes into the blur pass, blurs the current hdr values several times to create a nice
    /// Bloom efect, then dispatches a compute shader to get the current average lumiosity,
    /// after that renders a fullscreen image which combines the ldr and hdr fragments as well
    /// as does tone mapping, and writes the output to the swapchain image.
    pub fn do_post_process<I>(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        frame_system: &FrameSystem,
        target_image: I
    )where I: ImageAccess + ImageViewAccess + Clone + Send + Sync + 'static{
        //First find the exposure we'll use for the rest of this frame
        self.physical_camera.compute_lumiosity(command_buffer, frame_system);

        //Now sort out the too bright texels and blur them for a blur effect
        self.bloom_system.execute_blur(
            command_buffer,
            frame_system,
            self.screen_sampler.clone(),
            &self.physical_camera
        );

        //Now we are ready to assemble our image by changing into the assemble pass
        self.assemble_image(command_buffer, frame_system, target_image);
    }

    ///Is an intermedia step which only operate on the opaque image. It takes some images
    ///and calculates and ao image from them
    pub fn calculate_ao(
        &mut self,
        command_buffer: &mut JkCommandBuff,
        frame_system: &FrameSystem,
        asset_manager: &mut AssetManager,
    ){
        self.fullscreen_system.do_ssao(command_buffer, frame_system, asset_manager);
    }


    ///Executes the post progress on the recived command buffer and returns it, returns the buffer
    /// unchanged if it is in the wrong stage.
    fn assemble_image<I>(&self,
        command_buffer: &mut JkCommandBuff,
        frame_system: &FrameSystem,
        target_image: I
    )where I: ImageAccess + ImageViewAccess + Clone + Send + Sync + 'static{
        //first change into the assemble pass
        let assemble_fb = frame_system.get_passes().assemble.get_fb_assemble(target_image);
        let clearings = vec![
            [0.0, 0.0, 0.0, 0.0].into()
        ];
        //Begin the assamble pass
        let local_cb = command_buffer.get_graphics_buffer();
        let mut new_cb = local_cb.begin_render_pass(assemble_fb, false, clearings)
        .expect("failed to start assemble pass");
        //now ready to do all the shading
        //create the descriptor set for the current image
        let blur = frame_system.get_passes().get_final_bloom_img();
        let final_image = frame_system.get_passes().get_final_image();
        let images = &frame_system.get_passes().images;

        let attachments_ds = PersistentDescriptorSet::start(self.pipeline.get_pipeline_ref(), 0) //at binding 0
            //albedo
            .add_sampled_image(
                images.gbuffer.albedo.clone(),
                self.screen_sampler.clone()
            )
            .expect("failed to add albedo to postprogress")
            //normal
            .add_sampled_image(
                images.gbuffer.normal.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add normal image to postprogress")
            //position
            .add_sampled_image(
                images.gbuffer.position.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add position image to postprogress")
            //roughness
            .add_sampled_image(
                images.gbuffer.roughness.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add roughness to postprogress")
            //metallic
            .add_sampled_image(
                images.gbuffer.metalness.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add metalness to postprogress")
            //depth
            .add_sampled_image(
                images.gbuffer.depth.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add depth to postprogress")
            //ao
            .add_sampled_image(
                images.post_images.final_ao.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add ao image to postprogress")
            //diffuse
            .add_sampled_image(
                images.ray_tracing.diffuse.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add diffuse to postprogress")
            //bloom img
            .add_sampled_image(
                blur,
                self.screen_sampler.clone()
            ).expect("failed to add bloom image to postprogress")
            //reflection img
            .add_sampled_image(
                images.ray_tracing.reflection.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add reflection image to postprogress")
            //shadow img
            .add_sampled_image(
                images.ray_tracing.shadows.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add shadow image to postprogress")
            //ray tracing debug img
            .add_sampled_image(
                images.ray_tracing.debug.clone(),
                self.screen_sampler.clone()
            ).expect("failed to add ray debug image to postprogress")
            //Actual final
            .add_sampled_image(
                final_image,
                self.screen_sampler.clone()
            ).expect("failed to add current final image to posprogress")
            //Build desc
            .build()
            .expect("failed to build postprogress cb");

        //the settings for this pass
        let settings = self.get_hdr_settings();

        let settings_buffer = PersistentDescriptorSet::start(self.pipeline.get_pipeline_ref(), 1) //At binding 1
            .add_buffer(settings)
            .expect("failed to add hdr image settings buffer to post progress attachment")
            .add_buffer(self.physical_camera.average_buffer.clone())
            .expect("failed to add lumiosity buffer to assemble pass")
            .build()
            .expect("failed to build settings attachment for postprogress pass");

        //perform the post progress
        new_cb = new_cb.draw(
            self.pipeline.get_pipeline_ref(),
            frame_system.get_dynamic_state(),
            vec![self.screen_vertex_buffer.clone()],
            (attachments_ds, settings_buffer),
            ()
        ).expect("failed to add draw call for the post progress plane");

        //Change back into neutral state
        new_cb = new_cb.end_render_pass().expect("failed to end assemble stage");

        command_buffer.set_graphics_buffer(new_cb);
    }

    ///Returns a vertexbuffer representing the screen.
    pub fn get_screen_vb(&self) -> Arc<vulkano::buffer::BufferAccess + Send + Sync>{
        self.screen_vertex_buffer.clone()
    }
}

///Blits one image to another. *IMPORTANT* The images have to fullfill the
///[requirements](https://docs.rs/vulkano/0.10.0/vulkano/command_buffer/struct.AutoCommandBufferBuilder.html#method.blit_image)
pub fn blit_images(
    command_buffer: AutoCommandBufferBuilder,
    source: Arc<ImageAccess + Send + Sync + 'static>,
    target: Arc<ImageAccess + Send + Sync + 'static>
) -> AutoCommandBufferBuilder{

    //Run some tests for error checking!

    let source_dim = ImageAccess::dimensions(&source);
    //Now same for the target
    let source_lower_right: [i32; 3] = {
        match source_dim{
            ImageDimensions::Dim2d{width, height, array_layers, cubemap_compatible} => [width as i32, height as i32, 1],
            _ => {
                println!("Faking image source dim");
                [1,1,1]
            }
        }
    };

    let target_dim = ImageAccess::dimensions(&target);
    //Now same for the target
    let target_lower_right: [i32; 3] = {
        match target_dim{
            ImageDimensions::Dim2d{width, height, array_layers, cubemap_compatible} => [width as i32, height as i32, 1],
            _ => {
                println!("Faking image destination");
                [1,1,1]
            }
        }
    };

    //Currently only resizeing single level, the hdr fragments to the blur image
    let local_cb = command_buffer.blit_image(
        source,
        [0; 3],
        source_lower_right,
        0,
        0,
        target,
        [0; 3],
        target_lower_right,
        0,
        0,
        1,
        Filter::Linear
    ).expect("failed to blit image");

    local_cb
}
