use std::sync::{Arc, RwLock};

use vulkano::framebuffer::RenderPassAbstract;
use vulkano::device::Device;
use vulkano::image::StorageImage;
use vulkano::image::AttachmentImage;
use vulkano::format::Format;
use vulkano::framebuffer::FramebufferAbstract;
use vulkano::image::traits::ImageViewAccess;
use vulkano;

use core::engine_settings::EngineSettings;
use render::renderer::JkQueues;
///Handles the forward object rendering as well as the sorting of hdr elements into a second hdr fragments only buffer
pub mod object_pass;

///A pass which can blur an image, is used for the bloom images.
pub mod blur_pass;

///Can only clear an image
pub mod single_image_pass;

///Takes the postprogressed image and the hdr image of the object pass, does tonemapping and assembles the image into a
/// final image which will be written to the swapchain image and later displayed.
pub mod assemble_pass;
///Collects all images needed in one render pass.
pub mod render_targets;

///Collects the used formats
#[derive(Copy, Clone)]
pub struct UsedFormats {
    //Gbuffer formats
    pub albedo: Format,
    pub depth: Format,
    pub normal: Format,
    pub position: Format,
    pub roughness: Format,
    pub metallness: Format,
    pub ambient: Format,
    //RayTracingImages
    pub diffuse: Format,
    pub reflection: Format,
    pub shadows: Format,
    pub debug_info: Format,

    //PostProcess Images
    pub bloom: Format,

    pub swapchain: Format
}


///A collection of the available render pass definitions.
#[derive(Clone)]
pub struct RenderPasses {

    //Local copy of the settings needed for fast rebuilding
    settings: Arc<RwLock<EngineSettings>>,
    device: Arc<Device>,
    queue: JkQueues,

    ///Renders the objects in a forward manor, in a second pass the msaa is resolved and the image
    /// is split in a hdr and ldr part.
    pub object_pass: object_pass::ObjectPass,
    ///Blurs the first texture and writes it blured based on settings to the output
    pub blur_pass: blur_pass::BlurPass,
    ///Takes all the generated images and combines them to the final image
    pub assemble: assemble_pass::AssemblePass,

    ///Collects all possible passes which only write to one image
    pub simple_passes: Vec<(Format, single_image_pass::SimplePass)>,

    ///Collects all images used in the passes.
    pub images: render_targets::ImageStore,

    //Holds all the used formats
    pub formats: UsedFormats,
}



impl RenderPasses{
    pub fn new(
        device: Arc<Device>,
        queue: JkQueues,
        swapchain_format: Format,
        settings: Arc<RwLock<EngineSettings>>
    ) -> Self{

        //Trying to find an albedo format
        let albedo_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R16G16B16A16Sfloat,
            vulkano::format::Format::R16G16B16A16Sfloat
        );

        let depth_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::D32Sfloat,
            vulkano::format::Format::D16Unorm
        );

        let normal_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R16G16B16A16Sfloat,
            vulkano::format::Format::R16G16B16A16Sfloat
        );

        let position_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R32G32B32A32Sfloat,
            vulkano::format::Format::R16G16B16A16Sfloat
        );
        //TODO Check if that is enough, but should, metallic and rough are always between 0/1.
        let metallic_and_roughness_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R8Unorm,
            vulkano::format::Format::R8Unorm
        );
        //TODO check as well
        let ao_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R8Unorm,
            vulkano::format::Format::R8Unorm
        );

        let bloom_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R32G32B32A32Sfloat,
            vulkano::format::Format::R16G16B16A16Sfloat
        );

        let diffuse_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R32G32B32A32Sfloat,
            vulkano::format::Format::R16G16B16A16Sfloat
        );

        let reflection_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R32G32B32A32Sfloat,
            vulkano::format::Format::R16G16B16A16Sfloat
        );



        let shadows_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R8Unorm,
            vulkano::format::Format::R8Unorm
        );

        let debug_info_format = get_supported_format(
            device.clone(),
            vulkano::format::Format::R16Sfloat,
            vulkano::format::Format::R16Sfloat
        );

        let formats = UsedFormats{
            albedo: albedo_format,
            depth: depth_format,
            normal: normal_format,
            position: position_format,
            roughness: metallic_and_roughness_format,
            metallness: metallic_and_roughness_format,
            ambient: ao_format,

            diffuse: diffuse_format,
            reflection: reflection_format,
            shadows: shadows_format,
            debug_info: debug_info_format,

            bloom: bloom_format,

            swapchain: swapchain_format
        };


        let object_pass = object_pass::ObjectPass::new(device.clone(), formats);
        let blur_pass = blur_pass::BlurPass::new(device.clone(), formats);
        let assemble = assemble_pass::AssemblePass::new(device.clone(), formats);


        let images = render_targets::ImageStore::new(
            settings.clone(),
            device.clone(),
            queue.clone(),
            formats
        );


        RenderPasses{
            settings,
            device,
            queue,
            object_pass: object_pass,
            blur_pass: blur_pass,
            assemble: assemble,
            simple_passes: Vec::new(),
            images,

            formats,
        }
    }
    ///Rebuilds the resolution dependent images
    pub fn rebuild_resolution_images(&mut self){
        self.images.recreate_res_dependent();
    }

    ///Returns the render pass and its subpass id for this configuratiuon
    pub fn conf_to_pass(&mut self, conf: RenderPassConf) -> (Arc<RenderPassAbstract + Send + Sync>, u32){
        match conf{
            RenderPassConf::ObjectPass => (self.object_pass.render_pass.clone(), 0),
            RenderPassConf::BlurPass => (self.blur_pass.render_pass.clone(), 0),
            RenderPassConf::AssemblePass =>(self.assemble.render_pass.clone(), 0),
            RenderPassConf::SimplePass(wanted_format) => {
                //Search for a pass with that format, otherwise, create one
                //TODO make faster
                for (format, pass) in self.simple_passes.iter(){
                    if *format == wanted_format{
                        return (pass.render_pass.clone(), 0);
                    }
                }

                println!("DEBUG: Creating Renderpass for format: {:?}", wanted_format);
                //Seams like we did not find the pass, lets create a new one and return that.
                let pass = single_image_pass::SimplePass::new(self.device.clone(), wanted_format);
                let abst_pass = pass.render_pass.clone();
                //Push into library
                self.simple_passes.push((wanted_format, pass));
                (abst_pass, 0)
            },
        }
    }

    ///Returns the image after the diffuse stage where light is already applied
    pub fn get_diffuse_image(&self) -> Arc<StorageImage<Format>>{
        //TODO actually implement
        self.images.ray_tracing.diffuse.clone()
    }

    ///Returns the image used for the post progress effects like eye adaption and Bloom.
    pub fn get_final_shaded_image(&self) -> Arc<StorageImage<Format>>{
        //TODO Change to the actual one
        self.images.ray_tracing.diffuse.clone()
    }

    ///Returns the framebuffer for the forward pass opaque
    pub fn get_gbuffer_framebuff(&self) -> Arc<FramebufferAbstract + Send + Sync>{
        //Create the object pass frame buffer
        let buffer = Arc::new(
            vulkano::framebuffer::Framebuffer::start(self.object_pass.render_pass.clone())
            //the albedo
            .add(self.images.gbuffer.albedo.clone()).expect("failed to add albedo image")
            //the depth
            .add(self.images.gbuffer.depth.clone()).expect("failed to add depth buffer")
            //The roughness
            .add(self.images.gbuffer.roughness.clone()).expect("failed to add roughness image")
            //The metallic
            .add(self.images.gbuffer.metalness.clone()).expect("failed to add metallic image")
            //The normal
            .add(self.images.gbuffer.normal.clone()).expect("failed to add normal image")
            //The pos
            .add(self.images.gbuffer.position.clone()).expect("failed to add pos image!")
            //The ao
            .add(self.images.gbuffer.ambient.clone()).expect("failed to add ao image")
            .build()
            .expect("failed to build main framebuffer!")
        );
        buffer
    }


    ///Returns the final blur image. *This could be not the first image in the bloom stack!*
    pub fn get_final_bloom_img(&self) -> Arc<StorageImage<Format>>{
        let biggest_blured_image = {
            self.settings
            .read().expect("failed to lock settings")
            .get_render_settings().get_bloom().first_bloom_level as usize
        };
        self.images.post_images.scaled_hdr[biggest_blured_image].final_image.clone()
    }

    ///Returns the final image of the opaque pass after adding all optional effects
    pub fn get_final_image(&self) -> Arc<StorageImage<Format>>{
        self.images.ray_tracing.diffuse.clone()
    }

    ///Returns the framebuffer to blur the input image at `idx` to the blur_h image
    pub fn get_framebuff_blur_h(&self, idx: usize) -> Arc<FramebufferAbstract + Send + Sync>{
        self.images.get_fb_blur_h(
            idx,
            self.blur_pass.render_pass.clone()
        )
    }

    ///Returns the framebuffer to blur the blur_h image at `idx` to the final image
    pub fn get_framebuff_blur_final(&self, idx: usize) -> Arc<FramebufferAbstract + Send + Sync>{
        self.images.get_fb_to_final(
            idx,
            self.blur_pass.render_pass.clone()
        )
    }

    ///Returns a framebuffer for the specified image and its format wich can be clear it and possibly write to it.
    pub fn get_framebuff_simple(
        &mut self,
        image: Arc<ImageViewAccess + Send + Sync>
    ) -> Arc<FramebufferAbstract + Send + Sync>{
        //Make sure there is a pass for that framebuffer
            //pass, id
        let (pass, _) = self.conf_to_pass(RenderPassConf::SimplePass(image.format()));
        Arc::new(
            vulkano::framebuffer::Framebuffer::start(pass)
            //Currently only has this single shadow map
            .add(image).expect("failed add image to simple framebuffer")
            .build()
            .expect("failed to build clear framebuffer!")
        )
    }

}

/// Returns the wanted format if supported, otherwise returns the fallback.
/// *IMPORTANT*: The fallback should be supported by every vulkan implementation, otherwise the
/// application could crash.
pub fn get_supported_format(device: Arc<Device>, wanted: Format, fallback: Format) -> Format{
    match AttachmentImage::input_attachment(
        device, [1; 2], wanted
    ){
        Ok(_) => return wanted,
        Err(_) => {
                println!("WARNING: Format:  {:?} not supported, using: {:?}", wanted, fallback);
                return fallback;
        },
    }
}

///Enum listing all the render passes availabe. They need to be known since we build pipelines against them
/// later.
#[derive(PartialEq, Clone)]
pub enum RenderPassConf{
    ///Currently renders everything, from the objects to the post progress.
    ObjectPass,
    BlurPass,
    AssemblePass,
    SimplePass(Format)
}
