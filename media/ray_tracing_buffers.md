# Ideas I have for the ray tracing steps

GBuffer:
________________________________________________________________________________
Num|     R         G           B       A   Format    Comment
___|____________________________________________________________________________
1  |    Albedo    Albedo      Albedo      RGB32f    16bit since we can have > 1.0 values for emissive ?
2  |    Depth                             Depthf24
3  |    Roughness                         u8
4  |    Metalness                         u8
5  |    normal    normal      normal      RGBf32
6  |    AO                                
___|____________________________________________________________________________                 
Ray tracing buffers:
___|____________________________________________________________________________
1  |    Diffuse   Diffuse     Diffuse     RGBf32    Receives the lightning info
2  |    Shadow    Shadow_dist             RGf32     Recives the shadow info, to be blured
3  |    Shininess                         Rf32      Shininess for the reflection
4  |    Reflect   Reflect     Reflect     RGBf32    Mirror like reflection, to be blured
___|____________________________________________________________________________
PostProcess
___|____________________________________________________________________________
1  |    Bloom     Bloom       Bloom       RGBf32    Stack of n render targets
2  |    SSAO                              Rf32
3  |    Swap      Swap        Swap        DEFAULT

The Idea is to render a normal g buffer with albedo, normal and pbr info.

Then do the lightning calculation within a ray tracing step and save the outcome to the diffuse buffer.
The nice thing, we can have those ultra long specular highlights (sun on the sea), con: not so many lights anymore.
We also save the shadows to a second buffer, where we save the distance to the hit as well.

Then we blur the shadow buffer and add it later to the diffuse,
by subtracting the unblured shadows, and adding the blured ones on top.
Benefit: We only need one sample per light and get hopefully pretty nice soft shadows blured based on
distance to the object (and maybe normal).

We also save a shininess value in the diffuse stage which we use to add our (mirror) reflections on top of the
image. Here as well, blur nicely and choose the reflection based on the roughness.

# Things to think about:

## Transparency
Currently I did not mention transparency. The nicest thing would be to do a completely ray traced pass
for the transparent objects with multiple samples for milky glass etc. However, this won't work performance wise I guess.

## Ray traced AO:
Would look realy nice, but I guess that won't work performance wise as well.

## Indirect lighting
I guess it should be possible to do AO and indirect lighting in one step. However one would need soo many rays :(.
However, it would be nice to place an emitting surface in a level and get lighting from it.
A work around might be to create a list of emitting surfaces and trace only those. But we could do that while
calculating the diffuse/specular color as well...
