#version 450

///INS FROM VERTEX
//Vertex Shader Input
layout(location = 0) in vec3 v_normal;
layout(location = 1) in vec3 FragmentPosition;
layout(location = 2) in vec2 v_TexCoord;
layout(location = 3) in vec3 v_position;
layout(location = 4) in vec3 in_view_pos;
layout(location = 5) in mat3 v_TBN;
layout(location = 8) in vec3 in_screen_pos;



//Global uniforms
layout(set = 0, binding = 0) uniform Data {
  vec3 camera_position;
  mat4 model;
  mat4 view;
  mat4 proj;
  float near;
  float far;
  vec2 screen_res;
} u_main;

//TEXTURES
layout(set = 1, binding = 0) uniform sampler2D t_Albedo;
layout(set = 1, binding = 1) uniform sampler2D t_Normal;
layout(set = 1, binding = 2) uniform sampler2D t_Metall_Rough;
layout(set = 1, binding = 3) uniform sampler2D t_Occlusion;
layout(set = 1, binding = 4) uniform sampler2D t_Emissive;
//TEXTURE_USAGE
//Texture usage infos (!= 1 is "not used" for now)
layout(set = 2, binding = 0) uniform TextureUsageInfo {
  uint b_albedo;
  uint b_normal;
  uint b_metal;
  uint b_roughness;
  uint b_occlusion;
  uint b_emissive;
  uint b_is_masked;
} u_tex_usage_info;

//TEXTURE_FACTORS
//Linear Texture factors from the material
layout(set = 2, binding = 1) uniform TextureFactors {
  vec4 albedo_factor;
  vec3 emissive_factor;
  float max_emission;
  float normal_factor;
  float metal_factor;
  float roughness_factor;
  float occlusion_factor;
  float alpha_cutoff;
} u_tex_fac;


//==============================================================================
///outgoing final color
layout(location = 0) out vec4 out_albedo;
layout(location = 1) out float out_roughness;
layout(location = 2) out float out_metallic;
layout(location = 3) out vec4 out_normal;
layout(location = 4) out vec4 out_position;
layout(location = 5) out float out_ao;

vec4 albedo;
float metallic;
float roughness;
vec3 normal;

// ----------------------------------------------------------------------------
void main()
{
  if (u_tex_usage_info.b_albedo != 1) {
    albedo = u_tex_fac.albedo_factor;
  }else{
    //convert from srgb (lazy)
    albedo = texture(t_Albedo, v_TexCoord);// * u_tex_fac.albedo_factor;
    //before we do anything expensive, theck if that material is masked, if so,
    //return if the current albedo alpha value is below the alpha_cutoff
    if(u_tex_usage_info.b_is_masked != 0){
      if (u_tex_fac.alpha_cutoff > albedo.a){
        //noice... early return
        discard;
      }
    }
  }

  //Set metallic color
  if (u_tex_usage_info.b_metal != 1) {
    metallic = u_tex_fac.metal_factor;
  }else{
    metallic = texture(t_Metall_Rough, v_TexCoord).b * u_tex_fac.metal_factor;
  }

  //Set roughness color
  if (u_tex_usage_info.b_roughness != 1) {
    roughness = u_tex_fac.roughness_factor;
  }else{
    roughness = texture(t_Metall_Rough, v_TexCoord).g * u_tex_fac.roughness_factor;
  }

  //Set ao color
  float ao = 1.0;
  if (u_tex_usage_info.b_occlusion != 1) {
    ao = u_tex_fac.occlusion_factor;
  }else{
    ao = texture(t_Occlusion, v_TexCoord).r * u_tex_fac.occlusion_factor;
  }

  //Set emessive color
  vec3 emissive = vec3(0.0);
  if (u_tex_usage_info.b_emissive != 1) {
    emissive = vec3(u_tex_fac.emissive_factor * u_tex_fac.max_emission);
  }else{
    emissive = texture(t_Emissive, v_TexCoord).rgb * u_tex_fac.emissive_factor * u_tex_fac.max_emission;
  }

  //TODO implemetn emmessive
  if (u_tex_usage_info.b_normal != 1){
    //normal = vec3(u_tex_fac.normal_factor);
    //from three-rs
    normal = v_normal; //use the vertex normal
  }else {
    vec3 surf_normal_tex = texture(t_Normal, v_TexCoord).rgb;
    normal = normalize(v_TBN * ((surf_normal_tex * 2.0 - 1.0) * vec3(u_tex_fac.normal_factor, u_tex_fac.normal_factor, 1.0)));
  }
  normal = normalize(normal);

  //calculate viewspace normal
  vec4 tmp_norm = inverse(transpose(u_main.view)) * vec4(normal, 0.0);
  vec3 vs_normal = normalize(tmp_norm.xyz);
  //Fill rendertargets
  //Alebdo contains emmision value as well
  out_albedo = vec4(albedo.xyz + emissive, 1.0);
  out_roughness = roughness;
  out_metallic = metallic;
  out_normal = vec4(normal, 1.0);
  out_position = vec4(v_position, 1.0);
  out_ao = ao;
}
