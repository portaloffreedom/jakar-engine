#version 450

//input depth and normal used for the calculation
layout(set = 0, binding = 0) uniform sampler2D t_Ssao; //depth is on b
layout(set = 0, binding = 1) uniform ssao_fin_settings{
  float ssao_brightness;
} u_ssao_fin_settings;

//Get the uvs
layout(location = 0) in vec2 inter_coord;
layout(location = 1) in vec2 v_pos;


layout(location = 0) out float final_opaque;

void main(){
  vec3[] kernel = {
    vec3(0.0, 0.0, 0.5),
    vec3( 1.0, 1.0, 0.0625),
    vec3( 1.0, 0.0, 0.0625),
    vec3( 1.0, -1.0, 0.0625),
    vec3( 0.0, -1.0, 0.0625),
    vec3(-1.0, -1.0, 0.0625),
    vec3(-1.0, 0.0, 0.0625),
    vec3(-1.0, 1.0, 0.0625),
    vec3( 0.0, 1.0, 0.0625),
  };

  vec2 texel_size = 1.0 / textureSize(t_Ssao, 0);

  float ao = kernel[0].z * texture(t_Ssao, inter_coord).r;


  for(int i= 1; i<9; i++){
    ao += texture(t_Ssao, inter_coord + texel_size * kernel[i].xy).r * kernel[i].z;
  }

  float color = (1.0 - ao * u_ssao_fin_settings.ssao_brightness);
  //TODO implement mini blur as well as brighness clamp
  final_opaque = color;
}
