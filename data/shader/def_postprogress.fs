#version 450




//tries to get the input attachment
layout(set = 0, binding = 0) uniform sampler2D albedo;
layout(set = 0, binding = 1) uniform sampler2D normal;
layout(set = 0, binding = 2) uniform sampler2D pos;
layout(set = 0, binding = 3) uniform sampler2D roughness;
layout(set = 0, binding = 4) uniform sampler2D metallic;
layout(set = 0, binding = 5) uniform sampler2D depth;
layout(set = 0, binding = 6) uniform sampler2D ao;
layout(set = 0, binding = 7) uniform sampler2D diffuse;
layout(set = 0, binding = 8) uniform sampler2D bloom_img;
layout(set = 0, binding = 9) uniform sampler2D reflection;
layout(set = 0, binding = 10) uniform sampler2DArray shadow;
layout(set = 0, binding = 11) uniform sampler2D ray_trace_debug;

layout(set = 0, binding = 12) uniform sampler2D final_img;


//Get the uvs
layout(location = 0) in vec2 inter_coord;
layout(location = 1) in vec2 v_pos;


//outputs the fragment color
layout(location = 0) out vec4 FragColor;

//The inputs for the hdr -> ldr pass
layout(set = 1, binding = 0) uniform hdr_settings{
  float gamma;
  float near;
  float far;
  int sampling_rate;
  int show_mode;
  int shadow_idx;
}u_hdr_settings;

///Will hold the average lumiosity of this frame
layout(set = 1, binding = 1) buffer LumiosityBuffer{
  ///Contains the lumiosity of the last 100 frames
  float lum_histogram[100];
  uint last_idx;
  float exposure;
} u_lum_buf;

float linear_depth(float depth){
  float f= u_hdr_settings.far;
  float n = u_hdr_settings.near;
  float z = (2 * n) / (f + n - depth * (f - n));
  return z;
}

//Tool to produce the heat map
vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
  //MainDepth
  if (u_hdr_settings.show_mode == 0) {

    float depth_out = texture(depth, inter_coord).r; //per spec

    float z = linear_depth(depth_out);

    FragColor = vec4(vec3(z), 1.0);
    return;
  }

  //DebugGrid 1
  if (u_hdr_settings.show_mode == 1) {
    //shows a grid of all the attachments for debuging
    //color_input (unmapped)

    //ALBEDO
    if ((inter_coord.x < 0.5 && inter_coord.x > 0.0) && (inter_coord.y < 0.5 && inter_coord.y > 0.0)){
      //sample the image
      vec2 coords = (inter_coord * 2.0);
      FragColor = vec4(texture(albedo, coords).rgb, 1.0);
    }

    //NORMAL
    else if ((inter_coord.x < 1.0 && inter_coord.x > 0.5) && (inter_coord.y < 0.5 && inter_coord.y > 0.0)){
      //sample the image
      //shrink
      vec2 coords = inter_coord * 2.0;
      //offset to the right
      coords.x = coords.x - 1.0;
      FragColor = vec4(texture(normal, coords).rgb, 1.0);
    }

    //POSITION
    else if ((inter_coord.x < 0.5 && inter_coord.x > 0.0) && (inter_coord.y < 1.0 && inter_coord.y > 0.5)){
      //sample the image
      //shrink
      vec2 coords = inter_coord * 2.0;
      //offset down
      coords.y = coords.y - 1.0;
      FragColor = vec4(texture(pos, coords).rgb, 1.0);

      //LDR IMAGE
    }else if ((inter_coord.x < 1.0 && inter_coord.x > 0.5) && (inter_coord.y < 1.0 && inter_coord.y > 0.5)){
      vec2 coords = inter_coord * 2.0;
      //offset down
      coords.y = coords.y - 1.0;
      //offset to the right
      coords.x = coords.x - 1.0;
      FragColor = vec4(texture(pos, coords).rgb, 1.0);
    }
    return;
  }

  //MAIN DIFFUSE
  if (u_hdr_settings.show_mode == 2) {
    FragColor = vec4(texture(diffuse, inter_coord).rgb, 1.0);
    return;
  }
  //REFLECTION
  if (u_hdr_settings.show_mode == 3) {
    FragColor = vec4(texture(reflection, inter_coord).rgb, 1.0);
    return;
  }

  //GBufffer grid 1
  if (u_hdr_settings.show_mode == 4){
    //ROUGHNESS
    if ((inter_coord.x < 0.5 && inter_coord.x > 0.0) && (inter_coord.y < 0.5 && inter_coord.y > 0.0)){
      //sample the image
      vec2 coords = (inter_coord * 2.0);
      FragColor = vec4(vec3(texture(roughness, coords).r), 1.0);
    }

    //METALLIC
    else if ((inter_coord.x < 1.0 && inter_coord.x > 0.5) && (inter_coord.y < 0.5 && inter_coord.y > 0.0)){
      //sample the image
      //shrink
      vec2 coords = inter_coord * 2.0;
      //offset to the right
      coords.x = coords.x - 1.0;
      FragColor = vec4(vec3(texture(metallic, coords).r), 1.0);
    }

    //AO RAW
    else if ((inter_coord.x < 0.5 && inter_coord.x > 0.0) && (inter_coord.y < 1.0 && inter_coord.y > 0.5)){
      //sample the image
      //shrink
      vec2 coords = inter_coord * 2.0;
      //offset down
      coords.y = coords.y - 1.0;
      FragColor = vec4(vec3(texture(ao, coords).r), 1.0);
    //BLOOM LEVEL
    }else if ((inter_coord.x < 1.0 && inter_coord.x > 0.5) && (inter_coord.y < 1.0 && inter_coord.y > 0.5)){
      vec2 coords = inter_coord * 2.0;
      //offset down
      coords.y = coords.y - 1.0;
      //offset to the right
      coords.x = coords.x - 1.0;
      //If we came here, return nothing
      FragColor = vec4(texture(bloom_img, coords).rgb, 1.0);
    }
    return;
  }

  //GBufffer grid 2
  if (u_hdr_settings.show_mode == 5){
    //REFLECTION
    if ((inter_coord.x < 0.5 && inter_coord.x > 0.0) && (inter_coord.y < 0.5 && inter_coord.y > 0.0)){
      vec2 coords = (inter_coord * 2.0);
      FragColor = vec4(texture(reflection, coords).rgb, 1.0);
    }

    //SHADOW
    else if ((inter_coord.x < 1.0 && inter_coord.x > 0.5) && (inter_coord.y < 0.5 && inter_coord.y > 0.0)){
      vec2 coords = inter_coord * 2.0;
      coords.x = coords.x - 1.0;
      FragColor = vec4(vec3(texture(shadow, vec3(coords, u_hdr_settings.shadow_idx)).r), 1.0);
    }

    //RayDebugImage
    else if ((inter_coord.x < 0.5 && inter_coord.x > 0.0) && (inter_coord.y < 1.0 && inter_coord.y > 0.5)){
      vec2 coords = inter_coord * 2.0;
      coords.y = coords.y - 1.0;

      float iteration_counter = texture(ray_trace_debug, coords).x;
      //Store debug data to the debug image
      float hue = (iteration_counter/500.0f);
      //offset into range 0.3-1.0
      hue = hue * 0.7;
      hue = hue + 0.3;
      clamp(hue, 0.0, 1.0);
      vec3 rgb = hsv2rgb(vec3(hue, 1.0, 0.9));


      FragColor = vec4(rgb, 1.0);

    //NONE
    }else if ((inter_coord.x < 1.0 && inter_coord.x > 0.5) && (inter_coord.y < 1.0 && inter_coord.y > 0.5)){
      vec2 coords = inter_coord * 2.0;
      coords.y = coords.y - 1.0;
      coords.x = coords.x - 1.0;
      FragColor = vec4(vec3(0.0), 1.0);
    }
    return;
  }

//Actual post progress
//==============================================================================

  //Add the blur to the image
  vec3 hdrColor = texture(final_img, inter_coord).rgb;
  vec3 bloomColor = texture(bloom_img, inter_coord).rgb;

  hdrColor += bloomColor;

  //hdrColor += bloomColor; // additive blending
  // Exposure tone mapping
  vec3 mapped = vec3(1.0) - exp(-hdrColor * u_lum_buf.exposure);
  // Gamma correction
  mapped = pow(mapped, vec3(1.0 / u_hdr_settings.gamma));
  FragColor = vec4(mapped, 1.0);

}
